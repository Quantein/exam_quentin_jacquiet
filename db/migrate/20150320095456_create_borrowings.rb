class CreateBorrowings < ActiveRecord::Migration
  def change
    create_table :borrowings do |t|
      t.integer :member_id
      t.integer :album_id
      t.date :borrowing_date
      t.date :back_date

      t.timestamps
    end
  end
end
