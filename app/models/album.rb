class Album < ActiveRecord::Base
  belongs_to :artist
  has_many :borrowings
  validates :title, presence:true
  validates :release_date, presence:true
  validates :artist_id, presence:true
  validates :title, presence:true
end
