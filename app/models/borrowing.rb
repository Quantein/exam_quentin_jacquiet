class Borrowing < ActiveRecord::Base
  belongs_to :member
  belongs_to :album
  before_create do |time|
  time.borrowing_date = Time.now
  end
end
