json.array!(@borrowings) do |borrowing|
  json.extract! borrowing, :id, :member_id, :album_id, :borrowing_date, :back_date
  json.url borrowing_url(borrowing, format: :json)
end
